var Random = (function(){
	return {
		get : function(low,high){
			return Math.floor(Math.random() * (high - low) + low);
		}
	}
})();

module.exports = Random;

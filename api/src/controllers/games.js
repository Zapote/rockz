var db = require('../database');
var Game = require('../models/game');
var rnd = require("../random");

function get(req,res){
	var game = db.getGame(req.params.number);
	if(game==null){
		res.status(404);
    	res.send('404: Not Found');
	}else{
		res.json(game);
	}
}

function post(req, res){
	var body = req.body;
	var game = new Game(rnd.get(10000,99999), body.challenge, body.actionOne, body.actionTwo);
	
	db.addGame(game);
	res.json(game);
}

function put (req,res){
	var game = db.getGame(req.params.number);
	var body = req.body;
	if(game==null){
		res.status(404);
    	res.send('404: Not Found');
	}else{
		game.actionTwo = body.actionTwo;
		db.saveGame(game);
		res.json(game);
	}
}

function list(req, res){
	res.json(db.listGames());
}

module.exports.get = get;
module.exports.post = post;
module.exports.put = put;
module.exports.list = list;

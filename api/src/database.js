var Database = (function() {
   	var games = [];

    return {
        addGame: function(game){
    		games.push(game);
        },
        listGames: function(){
    		return games;
        },
        getGame: function(number){
        	var game = null;
        	games.forEach(function(g){
        		if(g.number == number){
        			game = g;
        			return;
        		}
        	});

        	return game;
        },
        saveGame:function(game){
    		games.forEach(function(g, i){
        		if(g.number == game.number){
        			games[i] = game;
        			games[i].refresh();
        			return;
        		}
        	});
        }
    }
})();

module.exports = Database;

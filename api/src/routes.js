module.exports = function(router){
    var games = require('./controllers/games');
    router.get('/games/:number', games.get);
    router.get('/games', games.list);
    router.post('/games', games.post);
    router.put('/games/:number', games.put);
}
var express = require('express')
var bodyParser = require('body-parser')
 
var app = express();
var cors = require('cors');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(function (req, res, next) {
  var start = Date.now();
  next();
  var stop = Date.now();
  console.log('%s %s %d ms',req.method,req.url, stop-start);
});

var router = express.Router();
require('./routes')(router);

app.use("/",router);

app.get('/', function(req, res) {
  res.send('Rockz Rest API\n');
});


app.listen(1337);

console.log('Server running at http://localhost:1337/');

var Game = function(number, challenge, actionOne, actionTwo){
	var mod = this;
	this.number = number;
	this.challenge = challenge;
	this.actionOne = actionOne;
	this.actionTwo = actionTwo;
	
	
	function getResult(){

		if(mod.actionOne == null || mod.actionTwo == null){
			return "In progress."
		}

		var actions = {
			rock :{ beats: "scissors", name: "rock"},
			scissors :{ beats: "paper", name: "scissors"},
			paper : { beats: "rock", name: "paper"}
		}


		var actionOne = actions[mod.actionOne];
		var actionTwo = actions[mod.actionTwo];


		if(actionOne.beats === actionTwo.name){
			return "Challenger wins! " + actionOne.name + " beats " + actionTwo.name + " Challenged must: " + mod.challenge;
		}

		if(actionTwo.beats === actionOne.name){
			return "Challenged wins! " + actionTwo.name + " beats " + actionOne.name + " Challenger must: " + mod.challenge;
		}


		return "Its a tie!";
	}
	this.result = getResult();
	this.refresh = function(){
		this.result = getResult();
	}

}

module.exports = Game;
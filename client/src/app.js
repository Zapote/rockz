export class App{


  configureRouter(config, router) {
    this.router = router;
    config.title = 'Rokkz';
    config.options.pushState = true;
    config.options.root = '/';
    config.map([
      { route: ['',':game'], name: 'home', moduleId: 'home/index' },
      { route: "not-found", name: '404', moduleId: '404' }
      ]);

    config.mapUnknownRoutes('not-found');
  }
}
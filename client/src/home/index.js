import { inject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-http-client';
import { Router } from 'aurelia-router';

@inject(HttpClient, Router)
export class Home {
  challenge = 'Köpa mjölk'

  constructor(http, router) {
    http.configure(config => {
      config.withBaseUrl('http://46.38.160.6:3000/')
    });
    this.http = http;
    this.router = router;
  }

  activate(params, navigationInstruction) {
    this.gameId = params.game;
    if (this.gameId) {
      this.fetchGame();
    }
  }

  fetchGame() {
    console.log('fetch-game')
    this.http.get('games/' + this.gameId)
      .then(x => JSON.parse(x.response))
      .then(a => {
        this.gameObject = a;
        this.challenge = this.gameObject.challenge;
      });
  }

  selectAction(action) {
    if (this.gameId != null) {
      let game = { 'actionTwo': action };
      this.http.put('games/' + this.gameId, game)
        .then(httpResponse => this.router.navigate("/" + this.gameId));
    } else {
      let game = { 'actionOne': action, 'challenge': this.challenge };
      this.http.post('games', game)
        .then(httpResponse => JSON.parse(httpResponse.response))
        .then(data => this.router.navigate("/" + data.number));
    }
  }
}
